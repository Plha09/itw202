import {useState} from "react"
import { TextInput, View, StyleSheet, Text}  from "react-native";
const TextInputComponent = () =>{
    const [text, setText] = useState('What is your name?');
    return (
        <View style = {styles.container}>  
        <Text>{text}</Text> 
        
            <TextInput onChangeText={inputValue=> setText(<Text>Hi {inputValue} from Gyalpozhing College of Information Tecgnology</Text>)}
           
           style={styles.inputName}/>
        </View>
    )
}
export default TextInputComponent
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputName:{
        borderWidth:0.5,
        borderColor: 'black',
        height: 30,
        width: 300,
        
    }
})