import {View, TextInput, StyleSheet, Alert, Dimensions, Text, useWindowDimensions, KeyboardAvoidingView, ScrollView} from 'react-native';
import React, { useState } from 'react'
import PrimaryButton from '../components/UI/PrimaryButton'
import Title from '../components/UI/Title';
import InstructionText from '../components/UI/InstructionText';
import Card from '../components/card';
import colors from '../constant/colors';


export default function StartGameScreen({onPickNumber}){
    //prop call bew da put it in {}
    const [enteredNumber, setEnteredNumber] = useState('');
    //example no. type bew da lok whole component will not reload, only this component will re -render

    const {width, height} = useWindowDimensions();
    function numberInputHandler(enteredText){
        setEnteredNumber(enteredText);
    }
    function resetInputHandler(){
        setEnteredNumber('')
    }

    function confirmInputHandler(){
        const chosenNumber=parseInt(enteredNumber)
            if(isNaN(chosenNumber) || chosenNumber < 1 || chosenNumber >99){
                Alert.alert('invalid Number!','number has to be between 1 and 99.', 
                [{text:'okay', style: 'destructive', onPress: resetInputHandler}])
                 return;
    }
    onPickNumber(chosenNumber)
}
const marginTopDistance = height < 380 ? 30:100;

    return (
        <ScrollView style ={styles.screen}>
            <KeyboardAvoidingView style = {styles.screen} behavior='position'>
        <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
            <Title style={styles.Title}>Guess My Number</Title>

    <Card style={styles.inputContainer}>
        
        <InstructionText>Enter a Number</InstructionText>
        <TextInput
            style={styles.numberInput}
            keyboardType='number-pad'
            maxLength={2}
            autoCapitalize='none'
            autoCorrect={false}
            value={enteredNumber}
            onChangeText = {numberInputHandler}
        />
        <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
            <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
        </View>
        <View style={styles.buttonContainer}>
        <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
        </View>
        </View>
    </Card>
    </View>
    </KeyboardAvoidingView>
    </ScrollView>
    )
}
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    screen:{
        flex: 1
    },

    rootContainer:{
        flex: 1,
        marginTop: deviceHeight < 380 ? 30:100,
        alignItems: 'center'

    },
    instructionText:{
        color: colors.accent500,
        fontSize:24,
    },
    inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 100,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: '#72063C',
    borderRadius: 8,
    elevation: 1,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
    },

    numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:'#DDB52F',
    borderBottomWidth: 2,
    color: '#DDB52F',
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
    },

    buttonsContainer: {
    flexDirection: 'row'
    },
    
    buttonContainer: {
    flex: 1,
    },
    
})
