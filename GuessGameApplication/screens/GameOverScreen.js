import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions, useWindowDimensions, ScrollView } from 'react-native';
import PrimaryButton from '../components/UI/PrimaryButton';

import Title from '../components/UI/Title';
import colors from '../constant/colors';

function GameOverScreen({roundsNumber, userNumber, onStartNewGame}){
    const {width, height} = useWindowDimensions();

    let imageSize = 400;
    if (width < 380){
        imageSize= 300;
    }
    if (height <400){
        imageSize=100;
    }
    const imageStyle= {
        width: imageSize,
        height: imageSize,
        borderRadius: imageSize/2
    }
    return (
        <ScrollView style = {styles.screen}>
        <View style = {styles.rootContainer}>
            <Title> GAME OVER!</Title>
            <View style = {[styles.imageContainer, imageStyle]}>
            <Image 
                style={styles.image}
                source = {require('../assets/image/success.png')} />
            </View>
            <Text style = {styles.summaryText}>
                Your phone needed <Text style = {styles.highlight}> {roundsNumber} </Text> rounds to guess the number   
                <Text style = {styles.highlight}> {userNumber} </Text> 
            </Text>
            <PrimaryButton onPress={onStartNewGame}> Start New Game</PrimaryButton>
        </View>
        </ScrollView>
    );
}

export default GameOverScreen;
const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen:{
        flex: 1,
    },
    rootContainer:{
     flex: 1,
     padding: 24,
     alignItems: 'center',
     justifyContent: 'center',
 },
 imageContainer:{
     width: deviceWidth < 380 ? 250:300,
     height: deviceWidth < 380 ? 250:300,
     borderRadius: deviceWidth < 380 ? 75:150,
     borderWidth: 3,
     borderColor: colors.primary800,
     overflow: 'hidden',
     margin: 36,
 },
 image:{
     height: ' 100%',
     width: '100%'
 },
 summaryText:{
     fontSize:24,
     textAlign: 'center',
     color: 'white'
 },
 highlight:{
     color: 'red'
 }
})
