import React, {useState, useEffect} from 'react'
import { View, StyleSheet, Text, Alert, FlatList,useWindowDimensions } from 'react-native'
import NumberContainer from '../components/game/NumberContainer';
import Card from '../components/card';
import InstructionText from '../components/UI/InstructionText';
import PrimaryButton from '../components/UI/PrimaryButton'
import Title from '../components/UI/Title'
import GuessLogItem from '../components/game/GuessLogItem'
import { Ionicons } from '@expo/vector-icons'

function generateRandomNumberBetween(min, max, exclude) {
  const rndNum = Math.floor(Math.random()*(max-min)) + min;

  if (rndNum == exclude) {
    return generateRandomNumberBetween(min, max, exclude)
  } else {
    return rndNum
  }
  
}
let minBoundary = 1;
let maxBoundary = 100;
export default function GameScreen({number, onGameOver}) {
    const initialGuess = generateRandomNumberBetween(1, 100, number)
    const [currentGuess, setCurrentGuess] = useState(initialGuess)
    const [guessRounds, setGuessRounds] = useState([initialGuess])
    const {width, height} = useWindowDimensions();
    useEffect(() => {
      if (currentGuess === number){
        onGameOver(guessRounds.length);
      }
    }, [currentGuess, number, onGameOver]);

    useEffect(() => {
      minBoundary = 1;
      maxBoundary = 100;
    }, [])
    function nextGuessHandler(direction) {
      if ((direction === 'lower' && currentGuess < number) || (direction === 'greater' && currentGuess > number)) {
        Alert.alert("Don't Lie!", "You know this is wrong...", [{text: 'sorry', style: 'cancel'}])
        return;
      }
      if (direction === 'lower') {
        maxBoundary = currentGuess;
       
      }
      else{
        minBoundary = currentGuess + 1;
      }
      console.log(minBoundary,maxBoundary)
      const newrndNumber = generateRandomNumberBetween(minBoundary, maxBoundary, currentGuess)
      setCurrentGuess(newrndNumber)
      setGuessRounds((prevGuessRounds => [newrndNumber, ...prevGuessRounds]))
    }
    const guessRoundsListLength = guessRounds.length

    let content = 
    <>
     <NumberContainer>{currentGuess}</NumberContainer>
        <Card>
        <InstructionText style = {styles.instructionText}>Higher or Lower?</InstructionText>
        <View style={styles.buttonsContainer}>
          <View style = {styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
              <Ionicons name= "md-add" size={24} color='white'/> 
           </PrimaryButton>
          </View>
          <View style = {styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
              <Ionicons name="md-remove" size={24} color= 'white'/>  
          </PrimaryButton>
          </View>
        </View>
      </Card>
    </>

    if (width> 500) {
      content = ( //fragment
        <> 
        <View style={styles.buttonsContainerWide}>
          <View style = {styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
              <Ionicons name= "md-add" size={24} color='black'/> 
           </PrimaryButton>
          </View>
          <NumberContainer>{currentGuess}</NumberContainer>
          <View style = {styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
              <Ionicons name="md-remove" size={24} color= 'white'/>  
          </PrimaryButton>
          </View>
        </View>
        </>
      )
    }
  return (
    <View style={styles.screen}>
       <Title> Opponent's Guess </Title>
       {content}
      <View style = {styles.listContainer}>
        <FlatList 
          data={guessRounds}
          renderItem = {(itemData) => 
            <GuessLogItem 
              roundNumber ={guessRoundsListLength - itemData.index}
              guess = {itemData.item}
            />}
              keyExtractor={(item) => item}
          />
      </View>
    </View>
  )

}
const styles = StyleSheet.create({
  screen:{
      flex: 1,
      padding: 24,
      alignItems:'center'
     
  },
  buttonsContainer:{
    flexDirection: 'row'
  },
  buttonContainer:{
    flex: 1
  },
  instructionText:{
    marginBottom: 12,
    
  },
  listContainer:{
    flex: 1,
    padding: 12
  },
  buttonsContainerWide:{
    flexDirection:'row',
    alignItems: 'center'
  },
  

})
