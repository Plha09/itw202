import { LinearGradient } from 'expo-linear-gradient';
import { StatusBar } from 'expo-status-bar';
import { Image, ImageBackground, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import StartGameScreen from './screens/StartGameScreen';
import { useState } from 'react';
import GameScreen from './screens/GameScreen';
import colors from './constant/colors';
import GameOverScreen from './screens/GameOverScreen';
import { useFonts } from 'react';
import AppLoading from 'expo-app-loading';


export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRounds, setGuessRounds] = useState(0);
  
  // useFonts({
  //   'open-sans-bold' : require('./assets/fonts/OpenSans-Bold.ttf'),
  //   'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
  // })

  // if(!fontsLoaded){
  //   return <AppLoading/>
  // }
  function pickedNumberHandler(pickerNumber) {
    setUserNumber(pickerNumber)
    setGameIsOver(false);
  }

  function gameOverHandler(numberOfRounds) {
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
    if (userNumber){
      screen = <GameScreen number = {userNumber} onGameOver = {gameOverHandler}/>
    }
    if (gameIsOver && userNumber) {
      screen = <GameOverScreen 
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
        />
    }
  return (
    <>
    <StatusBar style= 'dark'/>
    <LinearGradient 
      style={styles.container} 
      colors={[colors.primary700, colors.accent500]}>
        <ImageBackground 
        source={require('./assets/image/background.png')}
        resizeMode="cover"
        style={styles.rootScreen}
        imageStyle={styles.backgroundImage} >
      <SafeAreaView style={styles.container}>
        {screen}
        </SafeAreaView>
        </ImageBackground>
    </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,   
  },
  rootScreen:{
    flex: 1,
    },
    backgroundImage:{
      opacity:0.15
    }
});
