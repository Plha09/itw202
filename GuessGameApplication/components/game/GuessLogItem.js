import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constant/colors';

function GuessLogItem({roundNumber, guess}) {
  return (
    <View style={styles.listItem}>
      <Text style={styles.itemText}>#{roundNumber}</Text>
      <Text style={styles.itemText}>Opponent's Guess: {guess}</Text>
    </View>
  )
}

export default GuessLogItem;

const styles = StyleSheet.create({
    listItem:{
        borderColor:'black',
        borderWidth: 1,
        borderRadius: 40,
        padding: 12,
        marginVertical:8,
        backgroundColor:'#c83349',
        flexDirection:'row',
        justifyContent:'space-between',
        width: '100%',
        elevation:4,
        shadowColor:'white',
        shadowOffset: {width:0, height: 0},
        shadowOpacity: 0.25,
        shadowRadius: 3,
        color:'white'
    },
    itemText:{
        
        
    }
})