import { Pressable, StyleSheet, Text, View } from 'react-native'
import React  from 'react'

const PrimaryButton = ({children, onPress}) => {
  return (
    <View style={styles.buttonOuterContainer}>

        <Pressable android_ripple={{color:'#034f84'}}
            style ={({pressed}) => pressed?
            [styles.buttonInnerContainer, styles.pressed]:
            styles.buttonInnerContainer}
             onPress={onPress}>
        <Text style={styles.buttonText}>{children}</Text>
        </Pressable>
    </View>
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        borderRadius: 28,
        margin: 4,
        overflow:'hidden'
    },
    buttonInnerContainer:{
        backgroundColor:'#b5e7a0',
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2,
    },
    buttonText:{
        color:'black',
        textAlign: 'center'
    },
    pressed:{
        opacity: 0.75,
    }
})