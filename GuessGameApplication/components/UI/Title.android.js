import React from 'react'
import { View, Text, StyleSheet, Platform } from 'react-native' 
import colors from '../../constant/colors'

export default function Title({children}) {
  return (
    <View>
        <Text style={Styles.text}>{children}</Text>
    </View>
  )
}
const Styles = StyleSheet.create({
    text:{
      
        borderWidth: Platform.OS === 'android' ? 2:0,
        borderWidth: Platform.select({ios:0, android: 2}),
        borderColor: colors.basic,
        padding: '5%',
        color: colors.basic,
        fontSize: 24,
        textAlign:'center',
        maxWidth:'80%',
        width:300,
        borderRadius: 2,
        
      }
})
