import { StyleSheet, Text, View } from 'react-native'
import React, { children } from 'react'
import { Colors } from 'react-native/Libraries/NewAppScreen'

function InstructionText({children, style}){
  return <Text style={[styles.instructionText, style]}>{children}</Text>
}

export default InstructionText;

const styles = StyleSheet.create({
    instructionText:{
        color: 'white',
        fontSize:22
    }
})