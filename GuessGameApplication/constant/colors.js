const colors={
    primary500: '#72063c',
    primary600: '#640233',
    primary700: '#4c0329',
    primary800: '#3b021f',
    accent500: '#ddb52f',
    basic: '#ffffff',
    primary:'#f7cac9',
    accent100:'#b5e7a0',
    accent:'#80ced6',

}
export default colors;