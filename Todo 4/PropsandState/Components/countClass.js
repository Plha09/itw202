import React, {Component} from 'react'
import {Text, View, Button } from 'react-native'
class CountClass extends Component {
    // if the variable needs to be Changed inside class component, state is used
    // state d always in class components
    state = { count: 0 }
    // var count initializing to zero
    onPress = () => {
        this.setState({
            count: this.state.count + 1})}
render() {
    return (
    <View>
        <Text> You Clicked {this.state.count} times </Text>
        <Button
        onPress={this.onPress}
        // when clicked, onPress is called.
        title = "Count"/>
    </View> )}}
export default CountClass;