import React from "react";
import { Text, View } from 'react-native';
const Course = (props) => {
    return (
        <View>
            <Text>Course: {props.courseCode }</Text>
        </View>
    );
}
const Courses =() => {
    return (
        <View>
            <Course courseCode='ITW101'/>
            {/* courseCode= parentcomponent */}
            {/* props na once asigned, you cannot change. if you want to change, state use baygo */}
            <Course courseCode= 'ITW202' />
        </View>
    );
}
export default Courses;