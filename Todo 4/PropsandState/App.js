import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './Components/courses';
import CountClass from './Components/countClass';
export default function App() {
  return (
    <View style={styles.container}>
      <CountClass/>
      {/* <Courses></Courses> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
