import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View, ScrollView, TextInput, Image } from 'react-native';
import ViewComponent from './Components/viewComponents';
import TextComponent from './Components/TextComponent';
import ImageComponent from './Components/ImageComponent';
import TextInputComponent from './Components/TextInput';
export default function App() {
  return (

    // <ScrollView>
    //   <Text>Some Text</Text>
    //   <View>
    //     <Text>Some more text</Text>
    //     <Image
    //     sourse={{
    //       uri: 'https://picsum.photos/64/64',
    //     }}/>
    //   </View>
    //   <TextInput 
    //   defaultValue = "You can type here"/>
    //   <Button
    //   onPress={() =>{
    //     alert("You tapped the button!");
    //   }}
    //   title = "Press Me"/>
 
    // </ScrollView>
    // <Text numberOfLines={1}> Kuzu Zangpo la! Gyalpozhing College of Information Technology

    // </Text>
    <ViewComponent/>,
    <TextComponent/>,
    <ImageComponent/>,
    <TextInputComponent/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
