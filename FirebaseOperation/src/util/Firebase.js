 import { firebase } from '@firebase/app'

// const CONFIG = {
//   apiKey: "AIzaSyAjCpcNaW76G8BQgOox64oLb2fSNLDc5A0",
//   authDomain: "firestore-operation.firebaseapp.com",
//   projectId: "firestore-operation",
//   storageBucket: "firestore-operation.appspot.com",
//   messagingSenderId: "797777204161",
//   appId: "1:797777204161:web:6614ad49e97cf614025540"
// };

const CONFIG = {
  apiKey: "AIzaSyDFP5Suf-d_EkRCvBISxOWhwIn8YcPtVeg",
  authDomain: "todo20-1bcef.firebaseapp.com",
  databaseURL: "https://todo20-1bcef-default-rtdb.firebaseio.com",
  projectId: "todo20-1bcef",
  storageBucket: "todo20-1bcef.appspot.com",
  messagingSenderId: "462078475891",
  appId: "1:462078475891:web:6d6214981ecab041963406",
  measurementId: "G-6YV9VWTWNX"
}
 firebase.initializeApp(CONFIG)

 export default firebase;
