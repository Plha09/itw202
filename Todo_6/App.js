import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style ={styles.container}>
    <View style={styles.square1}><Text>1</Text></View>
    <View style={styles.square2}><Text>2</Text></View>
    <View style={styles.square3}><Text>3</Text></View>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // box zo ni
    flexDirection: "row",
    // box tsu row or column na zoni, column na zowa chin, align items d cross axis na apply jo wi
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: 60,
    paddingRight: 90
  },
  square1: {
    backgroundColor:'red',
    width: 55,
    height: 315,
    justifyContent: 'center',
    alignItems: 'center'
},
square2: {
  backgroundColor:'blue',
  width: 160,
  height: 315,
  justifyContent: 'center',
  alignItems: 'center'
},
square3: {
  backgroundColor:'green',
    width: 10,
    height: 315,
    justifyContent: 'center',
    alignItems: 'center'
},
});
