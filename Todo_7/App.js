import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style ={styles.container}>
    <View style={styles.square1}></View>
    <View style={styles.square2}></View>
    
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: 60,
    paddingRight: 90,
    marginTop:320
  },
  square1: {
    backgroundColor:'red',
    width: 150,
    height: 150,
    marginLeft: 50,
   
    justifyContent: 'center',
    alignItems: 'center'
},
square2: {
  backgroundColor:'blue',
  width: 150,
  height: 150,
  marginLeft: 50,
  justifyContent: 'center',
  alignItems: 'center'
},
});
