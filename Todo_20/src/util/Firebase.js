import { firebase } from '@firebase/app'

const firebaseConfig = {
  apiKey: "AIzaSyDFP5Suf-d_EkRCvBISxOWhwIn8YcPtVeg",
  authDomain: "todo20-1bcef.firebaseapp.com",
  projectId: "todo20-1bcef",
  storageBucket: "todo20-1bcef.appspot.com",
  messagingSenderId: "462078475891",
  appId: "1:462078475891:web:6d6214981ecab041963406",
  measurementId: "G-6YV9VWTWNX"
};
firebase.initializeApp(firebaseConfig)

export default firebase;
