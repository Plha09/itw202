import React from "react";
import { StyleSheet, View, Text } from "react-native";

const Com = () =>{
    const greeting = 'Getting started\nwith react\nnative!';
    // creating a variable called const, var and let ya tsu tub
    const name= 'My name is Pema\nLhamo';
    return (
        <View>
            <Text style={style.textStyle}>{greeting}</Text>
            <Text style={{fontSize:20, textAlign: 'center', paddingRight:80, fontWeight:'bold'}}>{name}</Text>
            {/* name d call bay yi const name nalay */}
        </View>
    )
};
const style = StyleSheet.create({
    textStyle:{
        fontSize:45,
        textAlign: 'center',
        paddingRight: 60,
        
    }
});
export default Com;