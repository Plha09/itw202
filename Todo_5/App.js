import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Com from './component/Com';

export default function App() {
  return (
    <View style={styles.container} >
    <Com/>
    </View>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    // flex gi screen divide bay ni 
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
